package com.example;

/**
 */
public interface CommandContext {
    <TResponse, TCommand> TResponse execute(TCommand command);
}
