package com.example;

import org.springframework.stereotype.Component;

/**
 */
@Component
public class GoodbyeCommandHandler implements CommandHandler<GoodbyeCommand, Integer> {
    @Override
    public Integer handle(GoodbyeCommand goodbyeCommand) {
        return 1;
    }
}
