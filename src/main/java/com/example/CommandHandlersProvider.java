package com.example;

/**
 */
public interface CommandHandlersProvider {
    CommandHandler<Object, Object> getHandler(Object command);
}
