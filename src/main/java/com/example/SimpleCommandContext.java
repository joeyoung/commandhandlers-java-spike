package com.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 */
@Component
public class SimpleCommandContext implements CommandContext {

    private CommandHandlersProvider handlersProvider;

    @Autowired
    public SimpleCommandContext(CommandHandlersProvider handlersProvider) {
        this.handlersProvider = handlersProvider;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <TResponse, TCommand> TResponse execute(TCommand command) {

        CommandHandler<Object, Object> handler = handlersProvider.getHandler(command);

        // add command validation

        Object response = handler.handle(command);

        return (TResponse)response;
    }
}
