package com.example;

/**
 */
public interface CommandHandler<TCommand, TResponse> {

    TResponse handle(TCommand command);

}
