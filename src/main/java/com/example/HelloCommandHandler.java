package com.example;

import org.springframework.stereotype.Component;

/**
 */
@Component
public class HelloCommandHandler implements CommandHandler<HelloCommand, String> {

    @Override
    public String handle(HelloCommand helloCommand) {
        return "Hello from Command Handler";
    }
}
