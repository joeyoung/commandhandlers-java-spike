package com.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 */
@Component
public class SpringCommandHandlersProvider implements CommandHandlersProvider, ApplicationListener<ContextRefreshedEvent> {

    private Map<Class<?>, String> handlers = new HashMap<Class<?>, String>();

    private ApplicationContext context;

    @Autowired
    public SpringCommandHandlersProvider(ApplicationContext context) {
        this.context = context;
    }

    @Override
    public CommandHandler<Object, Object> getHandler(Object command) {
        String name = handlers.get(command.getClass());
        if (name == null) {
            throw new RuntimeException("No command handler found for command: " + command.getClass());
        }

        CommandHandler<Object, Object> handler = context.getBean(name, CommandHandler.class);

        return handler;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        handlers.clear();

        String[] handlerNames = context.getBeanNamesForType(CommandHandler.class);
        for(String name : handlerNames) {
            Object handler = context.getBean(name);
            Class<?> commandType = getHandledCommandType(handler.getClass());

            handlers.put(commandType, name);
        }
    }

    private Class<?> getHandledCommandType(Class<?> clazz){
        Type[] genericInterfaces = clazz.getGenericInterfaces();
        ParameterizedType type = findByRawType(genericInterfaces, CommandHandler.class);
        return (Class<?>) type.getActualTypeArguments()[0];
    }

    private ParameterizedType findByRawType(Type[] genericInterfaces, Class<?> expectedRawType) {
        for (Type type : genericInterfaces) {
            if (type instanceof ParameterizedType) {
                ParameterizedType parametrized = (ParameterizedType) type;
                if (expectedRawType.equals(parametrized.getRawType())) {
                    return parametrized;
                }
            }
        }
        throw new RuntimeException();
    }
}
